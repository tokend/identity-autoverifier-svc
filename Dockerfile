FROM golang:1.12

WORKDIR /go/src/gitlab.com/tokend/identity-autoverifier-svc
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/identity-autoverifier-svc gitlab.com/tokend/identity-autoverifier-svc

###

FROM alpine:3.9

COPY --from=0 /usr/local/bin/identity-autoverifier-svc /usr/local/bin/identity-autoverifier-svc
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["identity-autoverifier-svc"]
