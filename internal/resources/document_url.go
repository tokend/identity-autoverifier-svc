package resources

import "gitlab.com/tokend/regources/generated"

// DocumentURLResponse represents the response on /documents identity API endpoint
type DocumentURLResponse struct {
	Data DocumentURL
}

// DocumentURL represents the "document_url" resource
type DocumentURL struct {
	regources.Key
	Attributes DocumentURLAttributes `json:"attributes"`
}

// DocumentURLAttributes - attributes of "document_url" resource
type DocumentURLAttributes struct {
	URL string `json:"url"`
}
