package resources

import (
	"gitlab.com/tokend/regources/generated"
)

// BlobResponse represents the response on /blobs identity API endpoint
type BlobResponse struct {
	Data Blob
}

// Blob represents the "blob" resource
type Blob struct {
	regources.Key
	Attributes BlobAttributes `json:"attributes"`
}

// BlobAttributes - attributes of the "blob" resource
type BlobAttributes struct {
	Value regources.Details `json:"value"`
}
