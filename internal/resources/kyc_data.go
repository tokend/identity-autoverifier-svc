package resources

type DocType string

const (
	PassportDocType        DocType = "passport"
	IdentityCardDocType    DocType = "identity_card"
	DrivingLicenseDocType  DocType = "driving_license"
	ResidencePermitDocType DocType = "residence_permit"
)

// KYCData - represents the KYC blob attached to change role request
type KYCData struct {
	FirstName   string    `json:"first_name"`
	LastName    string    `json:"last_name"`
	DateOfBirth string    `json:"date_of_birth"`
	Address     Address   `json:"address'"`
	Documents   Documents `json:"documents"`
}

func (d KYCData) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"first_name":    d.FirstName,
		"last_name":     d.LastName,
		"address":       d.Address,
		"documents":     d.Documents,
		"date_of_birth": d.DateOfBirth,
	}
}

// Address - represents KYC address details
type Address struct {
	Line1      string `json:"line_1"`
	Line2      string `json:"line_2"`
	City       string `json:"city"` // Use Detroit on Sandbox to receive failed result
	Country    string `json:"country"`
	State      string `json:"state"`
	PostalCode string `json:"postal_code"`
}

func (a Address) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"line_1":      a.Line1,
		"line_2":      a.Line2,
		"city":        a.City,
		"country":     a.Country,
		"state":       a.State,
		"postal_code": a.PostalCode,
	}
}

// Documents - represents KYC attached documents
type Documents struct {
	IDDocument IDDocument `json:"kyc_id_document"`
}

func (d Documents) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"id_document": d.IDDocument,
	}
}

// IDDocument - represents KYC ID document details
type IDDocument struct {
	FaceFile DocFile  `json:"face"`
	BackFile *DocFile `json:"back"`
	Type     DocType  `json:"type"`
}

func (d IDDocument) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"face": d.FaceFile,
		"back": d.BackFile,
		"type": d.Type,
	}
}

// DocFile - represents file details attached to KYC document
type DocFile struct {
	Name string `json:"name"`
	Key  string `json:"key"`
}

func (f DocFile) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"name": f.Name,
		"key":  f.Key,
	}
}
