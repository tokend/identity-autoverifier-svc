package resources

// TxData - represents params to be sent with tx submit call
type TxData struct {
	Tx string `json:"tx"`
}

// TxSuccessResponse - represents the response on successful tx submit call
type TxSuccessResponse struct {
	Hash     string `json:"hash"`
	Ledger   int32  `json:"ledger"`
	Envelope string `json:"envelope_xdr"`
	Result   string `json:"result_xdr"`
	Meta     string `json:"result_meta_xdr"`
}
