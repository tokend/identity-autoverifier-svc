package resources

import (
	"gitlab.com/tokend/regources/generated"
)

// IdentitiesResponse represents the response on /identities identity API endpoint
type IdentitiesResponse struct {
	Data  []Identity      `json:"data"`
	Links regources.Links `json:"links"`
}

// Identity represents the "identity" resource
type Identity struct {
	regources.Key
	Attributes IdentityAttributes `json:"attributes"`
}

// IdentityAttributes - represents the attributes of the "identity" resource
type IdentityAttributes struct {
	Email   string `json:"email"`
	Address string `json:"address"`
}
