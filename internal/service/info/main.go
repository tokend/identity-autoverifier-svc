package info

import (
	"encoding/json"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

type Client interface {
	Get(endpoint string) ([]byte, error)
}

type info struct {
	Passhprase         string `json:"network_passphrase"`
	AdminAccountID     string `json:"admin_account_id"`
	TxExpirationPeriod int64  `json:"tx_expiration_period"`
}

func Get(client Client) *info {
	responseBB, err := client.Get("/")
	if err != nil {
		panic(errors.Wrap(err, "failed to get horizon info"))
	}

	result := info{}

	err = json.Unmarshal(responseBB, &result)
	if err != nil {
		panic(errors.Wrap(err, "failed to unmarshal horizon info", logan.F{
			"response_bb":  responseBB,
			"response_str": string(responseBB),
		}))
	}

	return &result
}
