package reviewer

import (
	"encoding/json"
	"github.com/pkg/errors"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/regources/generated"
)

type changeRoleRequestQI interface {
	Get(id uint64) (*regources.ReviewableRequestResponse, error)
}

type txSubmitter interface {
	Submit(operations ...xdrbuild.Operation) error
}

type taskProvider interface {
	TaskManualReviewRequired() uint32
	TaskSubmitAutoVerification() uint32
	TaskCompleteAutoVerification() uint32
}

type requestReviewer struct {
	taskProvider        taskProvider
	txSubmitter         txSubmitter
	changeRoleRequestsQ changeRoleRequestQI
}

// New - returns new instance of requestReviewer
func New(
	txSubmitter txSubmitter,
	taskProvider taskProvider,
	changeRoleRequestsQ changeRoleRequestQI,
) requestReviewer {

	return requestReviewer{
		taskProvider:        taskProvider,
		txSubmitter:         txSubmitter,
		changeRoleRequestsQ: changeRoleRequestsQ,
	}
}

// Reject - submits an operation to reject the request
func (r requestReviewer) Reject(requestID uint64, rejectReason string, externalDetails map[string]string) error {
	return r.reviewRequest(requestID, reviewRequestOpts{
		action:          xdr.ReviewRequestOpActionReject,
		externalDetails: externalDetails,
		rejectReason:    rejectReason,
		tasksToRemove:   0,
		tasksToAdd:      0,
	})
}

// Reject - submits an operation to reject the request
func (r requestReviewer) SendToManualReview(requestID uint64, externalDetails map[string]string) error {
	return r.reviewRequest(requestID, reviewRequestOpts{
		action:          xdr.ReviewRequestOpActionApprove,
		tasksToRemove:   r.taskProvider.TaskCompleteAutoVerification() | r.taskProvider.TaskSubmitAutoVerification(),
		tasksToAdd:      r.taskProvider.TaskManualReviewRequired(),
		externalDetails: externalDetails,
		rejectReason:    "",
	})
}

// SendToCompleteAutoVerification - removes task to submit and sets task to complete auto verification
func (r requestReviewer) SendToCompleteAutoVerification(requestID uint64, externalDetails map[string]string) error {
	return r.reviewRequest(requestID, reviewRequestOpts{
		action:          xdr.ReviewRequestOpActionApprove,
		tasksToRemove:   r.taskProvider.TaskSubmitAutoVerification(),
		tasksToAdd:      r.taskProvider.TaskCompleteAutoVerification(),
		externalDetails: externalDetails,
		rejectReason:    "",
	})
}

// Approve - submits an operation to remove auto verification tasks and try to approve request. If there are any other
// pending tasks on the request, Approve won't remove them and request will remain pending
func (r requestReviewer) Approve(requestID uint64, externalDetails map[string]string) error {
	return r.reviewRequest(requestID, reviewRequestOpts{
		action:          xdr.ReviewRequestOpActionApprove,
		tasksToRemove:   r.taskProvider.TaskCompleteAutoVerification() | r.taskProvider.TaskSubmitAutoVerification(),
		tasksToAdd:      0,
		externalDetails: externalDetails,
		rejectReason:    "",
	})
}

type reviewRequestOpts struct {
	action          xdr.ReviewRequestOpAction
	rejectReason    string
	tasksToAdd      uint32
	tasksToRemove   uint32
	externalDetails map[string]string
}

func (r requestReviewer) reviewRequest(requestID uint64, opts reviewRequestOpts) error {
	response, err := r.changeRoleRequestsQ.Get(requestID)
	if err != nil {
		return errors.Wrap(err, "failed to get request by ID")
	}

	if opts.externalDetails == nil {
		opts.externalDetails = make(map[string]string)
	}
	externalDetailsBB, err := json.Marshal(&opts.externalDetails)
	if err != nil {
		return errors.Wrap(err, "failed to marshal external details")
	}

	operation := xdrbuild.ReviewRequest{
		ID:     requestID,
		Hash:   &response.Data.Attributes.Hash,
		Action: opts.action,
		Reason: opts.rejectReason,
		ReviewDetails: xdrbuild.ReviewDetails{
			TasksToAdd:      opts.tasksToAdd,
			TasksToRemove:   response.Data.Attributes.PendingTasks & opts.tasksToRemove,
			ExternalDetails: string(externalDetailsBB),
		},
		Details: changeRoleRequestDetails{},
	}

	return r.txSubmitter.Submit(operation)
}

type changeRoleRequestDetails struct{}

func (d changeRoleRequestDetails) ReviewRequestDetails() xdr.ReviewRequestOpRequestDetails {
	return xdr.ReviewRequestOpRequestDetails{
		RequestType: xdr.ReviewableRequestTypeChangeRole,
	}
}
