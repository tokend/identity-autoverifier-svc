package provider

import (
	"fmt"
	"net/url"

	"gitlab.com/tokend/identity-autoverifier-svc/internal/api"
	"gitlab.com/tokend/regources/generated"
)

const requestStatePending uint32 = 1

type changeRoleRequestsQI interface {
	GetList(api.QueryParams) (*regources.ReviewableRequestsResponse, error)
}

type taskProvider interface {
	TaskSubmitAutoVerification() uint32
	TaskCompleteAutoVerification() uint32
}

type requestProvider struct {
	taskProvider        taskProvider
	changeRoleRequestsQ changeRoleRequestsQI
}

// New - return new instance of requestProvider
func New(changeRoleRequestsQ changeRoleRequestsQI, taskProvider taskProvider) requestProvider {
	return requestProvider{
		changeRoleRequestsQ: changeRoleRequestsQ,
		taskProvider:        taskProvider,
	}
}

// NewRequestsBatch - returns the list of pending requests that are waiting for auto verification submit
func (p requestProvider) UnsubmittedBatch(cursor string) (*regources.ReviewableRequestsResponse, error) {
	return p.pendingBatch(p.taskProvider.TaskSubmitAutoVerification(), cursor)
}

// UncompletedRequestsBatch - returns the list of pending requests that are waiting for auto verification completion
func (p requestProvider) UncompletedBatch(cursor string) (*regources.ReviewableRequestsResponse, error) {
	return p.pendingBatch(p.taskProvider.TaskCompleteAutoVerification(), cursor)
}

func (p requestProvider) pendingBatch(pendingTasks uint32, cursor string) (*regources.ReviewableRequestsResponse, error) {
	params := url.Values{}

	params.Set("page[cursor]", cursor)
	params.Set("page[limit]", "100")
	params.Set("filter[state]", fmt.Sprintf("%d", requestStatePending))
	params.Set("filter[pending_tasks]", fmt.Sprintf("%d", pendingTasks))
	params.Set("include", "request_details")

	return p.changeRoleRequestsQ.GetList(params)
}
