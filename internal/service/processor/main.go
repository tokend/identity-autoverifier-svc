package processor

import (
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/idmind"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/resources"
)

const (
	extDetailsKeyProblem           = "problem"
	extDetailsKeyReputationProblem = "reputation_problem"
	extDetailsKeyTxID              = "idm_tx_id"

	problemFailedToBuildEvaluationParams = "Failed to build IDMind KYC evaluation params"
	problemNoTxID                        = "IDMind txID not found in external details"
	problemFailedToDeriveKYCData         = "Failed to derive KYC data"
	problemFailedToFetchIDDoc            = "Failed to fetch ID document"
)

type requestReviewer interface {
	Approve(requestID uint64, extDetails map[string]string) error
	Reject(requestID uint64, rejectReason string, extDetails map[string]string) error

	SendToManualReview(requestID uint64, extDetails map[string]string) error
	SendToCompleteAutoVerification(requestID uint64, externalDetails map[string]string) error
}

type documentsQI interface {
	GetUrl(documentId string) (*resources.DocumentURLResponse, error)
}

type blobsQI interface {
	Get(blobId string) (*resources.BlobResponse, error)
}

type identitiesQI interface {
	GetByAddress(string) (*resources.Identity, error)
}

type roleProvider interface {
	AccountRoleGeneral() uint32
	AccountRoleUsVerified() uint32
	AccountRoleUsAccredited() uint32
}

type taskProvider interface {
	TaskSubmitAutoVerification() uint32
	TaskManualReviewRequired() uint32
}

type requestProcessor struct {
	log *logan.Entry

	idmindConnector idmind.Connector
	reviewer        requestReviewer

	roleProvider roleProvider
	taskProvider taskProvider

	identitiesQ identitiesQI
	documentsQ  documentsQI
	blobsQ      blobsQI
}

func New(
	blobsQ blobsQI,
	documentsQ documentsQI,
	identitiesQ identitiesQI,
	reviewer requestReviewer,
	roleProvider roleProvider,
	taskProvider taskProvider,
	idmindConnector idmind.Connector,
	log *logan.Entry,
) requestProcessor {
	return requestProcessor{
		log: log,

		blobsQ:      blobsQ,
		documentsQ:  documentsQ,
		identitiesQ: identitiesQ,

		roleProvider: roleProvider,
		taskProvider: taskProvider,

		reviewer:        reviewer,
		idmindConnector: idmindConnector,
	}
}
