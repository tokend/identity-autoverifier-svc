package processor

import (
	"encoding/json"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/idmind"
	"gitlab.com/tokend/regources/generated"
)

func (p requestProcessor) ProcessUncompleted(request regources.ReviewableRequest, requestDetails regources.ChangeRoleRequest) error {
	fields := logan.F{}

	requestID := mustU64RequestID(request.ID)
	fields["request_id"] = requestID

	if (request.Attributes.PendingTasks & p.taskProvider.TaskManualReviewRequired()) != 0 {
		p.log.WithFields(fields).Debugf("request has manual review task unset, skipping")
		return nil
	}

	if (request.Attributes.PendingTasks & p.taskProvider.TaskSubmitAutoVerification()) != 0 {
		p.log.WithFields(fields).Debugf("request has submit auto verification task unset, skipping")
		return nil
	}

	txID, err := getIDMindTXId(request)
	if err != nil {
		return errors.Wrap(err, "failed to get IDMind txID", fields)
	}
	if txID == "" {
		p.log.
			WithFields(fields).
			Warn("IDMind tx ID not found in external details, sending to manual review")

		err := p.reviewer.SendToManualReview(requestID, map[string]string{
			extDetailsKeyProblem: problemNoTxID,
		})
		if err != nil {
			return errors.Wrap(err, "failed to send request to manual review", fields)
		}

		return nil
	}

	idmResponse, err := p.idmindConnector.CheckState(txID)
	if err != nil {
		return errors.Wrap(err, "failed to check IDMind request state", fields)
	}
	fields["idm_response"] = idmResponse

	rejectReason, rejectDetails := idmResponse.RejectReason()
	if rejectReason != "" {
		fields["reject_reason"] = rejectReason
		fields["reject_details"] = rejectDetails

		p.log.WithFields(fields).Info("IDMind returned the reject reason, rejecting the request")

		err := p.reviewer.Reject(requestID, rejectReason, rejectDetails)
		if err != nil {
			return errors.Wrap(err, "failed to reject request", fields)
		}

		return nil
	}

	if idmResponse.KYCState == idmind.AcceptedKYCState {
		p.log.WithFields(fields).Info("IDMind says the request is OK, approving")

		err = p.reviewer.Approve(requestID, nil)
		if err != nil {
			return errors.Wrap(err, "failed to approve kyc request", fields)
		}

		return nil
	}

	// Not fully reviewed yet
	if idmResponse.IsManualReview() {
		p.log.WithFields(fields).Info("request waits for IDMind manual review, skipping")
	}

	return nil
}

func getIDMindTXId(request regources.ReviewableRequest) (string, error) {
	externalDetailsBB := []byte(request.Attributes.ExternalDetails)

	var externalDetails struct {
		Data []map[string]string `json:"data"`
	}

	err := json.Unmarshal(externalDetailsBB, &externalDetails)
	if err != nil {
		return "", errors.Wrap(err, "failed to unmarshal external details")
	}

	var txID string
	var ok bool
	for _, extDetails := range externalDetails.Data {
		txID, ok = extDetails[extDetailsKeyTxID]
		if !ok {
			// No 'tx_id' key in these externalDetails.
			continue
		}
	}

	return txID, nil
}
