package processor

import (
	"encoding/json"
	"fmt"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/client"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/idmind"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/resources"
	"gitlab.com/tokend/regources/generated"
	"strconv"
)

func (p requestProcessor) ProcessUnsubmitted(request regources.ReviewableRequest, requestDetails regources.ChangeRoleRequest) error {
	fields := logan.F{}

	requestID := mustU64RequestID(request.ID)
	fields["request_id"] = requestID
	fields["account_role_to_set"] = requestDetails.Attributes.AccountRoleToSet

	if (request.Attributes.PendingTasks & p.taskProvider.TaskManualReviewRequired()) != 0 {
		p.log.WithFields(fields).Debugf("request has manual review task unset, skipping")
		return nil
	}

	switch requestDetails.Attributes.AccountRoleToSet {
	case
		uint64(p.roleProvider.AccountRoleGeneral()),
		uint64(p.roleProvider.AccountRoleUsAccredited()),
		uint64(p.roleProvider.AccountRoleUsVerified()):
		// it's okay, just go process it
	default:
		p.log.WithFields(fields).Debug("request has not interesting account role to set, skipping")
		return nil
	}

	account := requestDetails.Relationships.AccountToUpdateRole
	if account == nil {
		return errors.From(errors.New("request details has no account relation"), fields)
	}

	identity, err := p.identitiesQ.GetByAddress(account.Data.ID)
	if err != nil {
		return errors.Wrap(err, "failed to get account identity", fields)
	}
	if identity == nil {
		return errors.From(errors.New("account identity not found"), fields)
	}

	email := identity.Attributes.Email
	fields["email"] = email

	p.log.WithFields(fields).Debug("got account email")

	kycData, err := p.deriveKYCData(requestDetails)
	if err != nil {
		switch errors.Cause(err).(type) {
		case client.ServerError:
			return errors.Wrap(err, "failed to derive KYC data, because of server error", fields)
		default:
			p.log.
				WithError(err).
				WithFields(fields).
				Warn("failed to derive KYC data, sending to manual review")

			err := p.reviewer.SendToManualReview(requestID, map[string]string{
				extDetailsKeyProblem: problemFailedToDeriveKYCData,
			})
			if err != nil {
				return errors.Wrap(err, "failed to send request to manual review", fields)
			}

			return nil
		}
	}
	fields["kyc_data"] = kycData

	idDocument, err := p.fetchIDDocument(kycData.Documents.IDDocument)
	if err != nil {
		switch errors.Cause(err).(type) {
		case client.ServerError:
			return errors.Wrap(err, "failed to fetch ID document because of server error", fields)
		default:
			p.log.
				WithError(err).
				WithFields(fields).
				Warn("failed to fetch ID document, sending to manual review")

			err := p.reviewer.SendToManualReview(requestID, map[string]string{
				extDetailsKeyProblem: problemFailedToFetchIDDoc,
			})
			if err != nil {
				return errors.Wrap(err, "failed to send request to manual review", fields)
			}

			return nil
		}
	}

	kycEvaluationParams, err := idmind.NewKYCEvaluationParams(*kycData, *idDocument, email)
	if err != nil {
		p.log.
			WithError(err).
			WithFields(fields).
			Warn("failed to build KYC evaluation params, sending to manual review")

		err := p.reviewer.SendToManualReview(requestID, map[string]string{
			extDetailsKeyProblem: fmt.Sprintf("%s: %s: ", problemFailedToBuildEvaluationParams, err),
		})
		if err != nil {
			return errors.Wrap(err, "failed to send request to manual review", fields)
		}

		return nil
	}

	idmResponse, err := p.idmindConnector.SubmitKYCEvaluation(*kycEvaluationParams)
	if err != nil {
		return errors.Wrap(err, "failed to submit IDMind application", fields)
	}

	rejectReason, rejectDetails := idmResponse.GetImmediateRejectReason()
	if rejectReason != "" {
		p.log.
			WithFields(fields).
			Info("request is immediately rejected by IDMind, rejecting")

		err = p.reviewer.Reject(requestID, rejectReason, rejectDetails)
		if err != nil {
			return errors.Wrap(err, "failed to reject immediate rejected request", fields)
		}

		return nil
	}

	reputationProblem := idmResponse.ReputationProblem()
	if reputationProblem != "" {
		fields["reputation_problem"] = reputationProblem

		p.log.
			WithFields(fields).
			Warn("IDMind says the application is OK, but found reputation problem, sending to manual review")

		err := p.reviewer.SendToManualReview(requestID, map[string]string{
			extDetailsKeyReputationProblem: reputationProblem,
			extDetailsKeyTxID: idmResponse.TxID,
		})

		if err != nil {
			return errors.Wrap(err, "failed to send request to manual review", fields)
		}

		return nil
	}

	p.log.WithFields(fields).Info("attaching document to IDMind application")

	err = p.idmindConnector.UploadIDDocument(idmResponse.TxID, *idDocument)
	if err != nil {
		return errors.Wrap(err, "failed to upload ID document to ID mind", fields)
	}

	p.log.
		WithFields(fields).
		Info("application to IDMind submitted, sending request to complete auto verification")

	err = p.reviewer.SendToCompleteAutoVerification(requestID, map[string]string{
		extDetailsKeyTxID: idmResponse.TxID,
	})
	if err != nil {
		return errors.Wrap(err, "failed to send request to complete auto verification", fields)
	}

	return nil
}

func (p requestProcessor) deriveKYCData(requestDetails regources.ChangeRoleRequest) (*resources.KYCData, error) {
	var details creatorDetails
	err := json.Unmarshal(requestDetails.Attributes.CreatorDetails, &details)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal creator details")
	}

	if details.BlobID == "" {
		return nil, errors.New("request doesn't contain blob ID")
	}

	blob, err := p.blobsQ.Get(details.BlobID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get blob")
	}

	var kycData resources.KYCData

	err = json.Unmarshal(mustUnescapeJSON(blob.Data.Attributes.Value), &kycData)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal kyc data")
	}

	return &kycData, nil
}

type creatorDetails struct {
	BlobID string `json:"blob_id"`
}

func (p requestProcessor) fetchIDDocument(doc resources.IDDocument) (*idmind.IDDocument, error) {
	if doc.FaceFile.Key == "" {
		return nil, errors.New("no document key present in KYC data")
	}

	faceFile, err := p.getFile(doc.FaceFile.Key)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get face file")
	}

	var backFile []byte
	if doc.BackFile != nil {
		backFile, err = p.getFile(doc.BackFile.Key)
		if err != nil {
			return nil, errors.Wrap(err, "failed to get back file")
		}
	}

	result := idmind.NewIDDocument(
		doc.Type,
		faceFile, backFile,
		doc.FaceFile.Name, doc.FaceFile.Name,
	)

	return &result, nil
}

func mustU64RequestID(strID string) uint64 {
	u64ID, err := strconv.ParseUint(strID, 10, 64)
	if err != nil {
		panic(errors.New("failed to parse requestID"))
	}

	return u64ID
}

func mustUnescapeJSON(v []byte) []byte {
	s, err := strconv.Unquote(string(v))
	if err != nil {
		panic(errors.New("failed to unescape JSON string"))
	}

	return []byte(s)
}
