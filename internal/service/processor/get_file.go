package processor

import (
	"gitlab.com/distributed_lab/logan/v3/errors"
	"io/ioutil"
	"net/http"
	"strings"
)

func (p requestProcessor) getFile(documentKey string) ([]byte, error) {
	d, err := p.documentsQ.GetUrl(documentKey)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to get Face Document by ID from Horizon")
	}

	response, err := http.Get(fixDocURL(d.Data.Attributes.URL))
	file, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, errors.Wrap(err, "Failed to read faceFile response into bytes")
	}

	return file, nil
}

func fixDocURL(url string) string {
	return strings.Replace(url, `\u0026`, `&`, -1)
}
