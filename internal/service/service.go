package service

import (
	"context"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/service/info"
	"gitlab.com/tokend/keypair"
	"sync"
	"time"

	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/distributed_lab/running"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/api"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/client"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/config"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/idmind"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/service/kvprovider"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/service/processor"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/service/provider"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/service/reviewer"
	"gitlab.com/tokend/regources/generated"
)

// New builds and returns the new instance of the Service
func New(cfg config.Config, log *logan.Entry) Service {
	c := client.New(cfg.Api.URL, cfg.Api.Signer)

	systemInfo := info.Get(c)

	blobsQ := api.NewBlobsQ(c)
	keyValueQ := api.NewKeyValueQ(c)
	documentsQ := api.NewDocumentsQ(c)
	identitiesQ := api.NewIdentitiesQ(c)
	changeRoleRequestsQ := api.NewChangeRoleRequestsQ(c)

	txBuilder := xdrbuild.NewBuilder(systemInfo.Passhprase, systemInfo.TxExpirationPeriod)
	txSubmitter := api.NewTxSubmitter(
		c,
		cfg.Api.Signer,
		keypair.MustParseAddress(systemInfo.AdminAccountID),
		txBuilder,
		log,
	)
	idmindConnector := idmind.New(cfg.IDMind.URL.String(), cfg.IDMind.AuthKey)
	keyValueProvider := kvprovider.New(
		keyValueQ,
		cfg.KeyValue.AccountRoleGeneral,
		cfg.KeyValue.AccountRoleUsVerified,
		cfg.KeyValue.AccountRoleUsAccredited,
		cfg.KeyValue.TaskSubmitAutoVerification,
		cfg.KeyValue.TaskCompleteAutoVerification,
		cfg.KeyValue.TaskManualReviewRequired,
	)

	requestReviewer := reviewer.New(txSubmitter, keyValueProvider, changeRoleRequestsQ)
	requestProvider := provider.New(changeRoleRequestsQ, keyValueProvider)
	requestProcessor := processor.New(
		blobsQ,
		documentsQ,
		identitiesQ,
		requestReviewer,
		keyValueProvider,
		keyValueProvider,
		*idmindConnector,
		log,
	)

	return Service{
		log: log,

		cursor:    cfg.Api.Cursor,
		sleepTime: cfg.Runner.SleepTime,

		requestProvider:  requestProvider,
		requestProcessor: requestProcessor,
	}
}

type requestProcessor interface {
	ProcessUnsubmitted(request regources.ReviewableRequest, requestDetails regources.ChangeRoleRequest) error
	ProcessUncompleted(request regources.ReviewableRequest, requestDetails regources.ChangeRoleRequest) error
}

type requestsProvider interface {
	UnsubmittedBatch(cursor string) (*regources.ReviewableRequestsResponse, error)
	UncompletedBatch(cursor string) (*regources.ReviewableRequestsResponse, error)
}

type Service struct {
	log *logan.Entry

	cursor    string
	sleepTime time.Duration

	requestProvider  requestsProvider
	requestProcessor requestProcessor
}

const (
	runnerNameUnsubmittedRequestListener = "unsubmitted-request-listener"
	runnerNameUncompletedRequestListener = "uncompleted-request-listener"
)

func (s Service) Run(ctx context.Context) {
	s.log.Info("Starting.")

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		running.WithBackOff(
			ctx,
			s.log,
			runnerNameUnsubmittedRequestListener,
			func(ctx context.Context) error {
				return s.listen(ctx, s.requestProvider.UnsubmittedBatch, s.requestProcessor.ProcessUnsubmitted)
			},
			30*time.Second,
			30*time.Second,
			30*time.Second,
		)
		wg.Done()
	}()

	wg.Add(1)
	go func() {
		running.WithBackOff(
			ctx,
			s.log,
			runnerNameUncompletedRequestListener,
			func(ctx context.Context) error {
				return s.listen(ctx, s.requestProvider.UncompletedBatch, s.requestProcessor.ProcessUncompleted)
			},
			30*time.Second,
			30*time.Second,
			30*time.Second,
		)
		wg.Done()
	}()

	wg.Wait()

	s.log.Info("All runners stopped - stopping cleanly.")
}

type providerFn func(cursor string) (*regources.ReviewableRequestsResponse, error)
type processorFn func(request regources.ReviewableRequest, requestDetails regources.ChangeRoleRequest) error

func (s Service) listen(ctx context.Context, provide providerFn, process processorFn) error {
	cursor := s.cursor

	// Backoff handles infinite loop, but it won't handle moving the cursor,
	// that's why we should have it here too:
	for {
		fields := logan.F{}

		fields["sleep_time"] = s.sleepTime
		fields["cursor"] = cursor

		response, err := provide(cursor)
		if err != nil {
			return errors.Wrap(err, "failed to get list of requests requests", fields)
		}

		for _, request := range response.Data {
			fields["request_id"] = request.ID
			fields["pending_tasks"] = request.Attributes.PendingTasks
			fields["external_details"] = request.Attributes.ExternalDetails

			requestDetails := response.Included.MustChangeRoleRequest(
				*request.Relationships.RequestDetails.Data,
			)
			if requestDetails == nil {
				return errors.From(errors.New("request came without request details"), fields)
			}

			s.log.Debug("ready to process request", fields)

			err := process(request, *requestDetails)
			if err != nil {
				return errors.Wrap(err, "failed to process request", fields)
			}

			s.log.Debug("request processed, going for the next one", fields)
		}

		if len(response.Data) == 0 {
			s.log.WithFields(fields).Debug("no new requests to process, sleeping")
			time.Sleep(s.sleepTime)

			// We should always re-run from the beginning, to be able to
			// re-process previously rejected requests:
			cursor = s.cursor
		} else {
			cursor = response.Data[len(response.Data)-1].ID
		}
	}
}
