package kvprovider

import (
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/regources/generated"
	"sync"
)

type keyValueQI interface {
	Get(key string) (*regources.KeyValueEntryResponse, error)
}

// KVProvider provides entries by key stored in key/value storage
type KeyValueProvider struct {
	entries sync.Map

	keyValueQ keyValueQI

	keyAccountRoleGeneral           string
	keyAccountRoleUsVerified        string
	keyAccountRoleUsAccredited      string
	keyTaskSubmitAutoVerification   string
	keyTaskCompleteAutoVerification string
	keyTaskManualReviewRequired     string
}

// New - returns new instance of KVProvider
func New(
	keyValueQ keyValueQI,

	keyAccountRoleGeneral,
	keyAccountRoleUsVerified,
	keyAccountRoleUsAccredited,
	keyTaskSubmitAutoVerification,
	keyTaskCompleteAutoVerification,
	keyTaskManualReviewRequired string,
) *KeyValueProvider {

	return &KeyValueProvider{
		entries: sync.Map{},

		keyValueQ: keyValueQ,

		keyAccountRoleGeneral:           keyAccountRoleGeneral,
		keyAccountRoleUsVerified:        keyAccountRoleUsVerified,
		keyAccountRoleUsAccredited:      keyAccountRoleUsAccredited,
		keyTaskSubmitAutoVerification:   keyTaskSubmitAutoVerification,
		keyTaskCompleteAutoVerification: keyTaskCompleteAutoVerification,
		keyTaskManualReviewRequired:     keyTaskManualReviewRequired,
	}
}

// TaskSubmitAutoVerification - returns key stored in key/value storage for submit auto verification task
func (p *KeyValueProvider) TaskSubmitAutoVerification() uint32 {
	return p.mustUint32(p.keyTaskSubmitAutoVerification)
}

// TaskCompleteAutoVerification - returns key stored in key/value storage for complete auto verification task
func (p *KeyValueProvider) TaskCompleteAutoVerification() uint32 {
	return p.mustUint32(p.keyTaskCompleteAutoVerification)
}

// TaskManualReviewRequired - returns key stored in key/value storage for manual review required task
func (p *KeyValueProvider) TaskManualReviewRequired() uint32 {
	return p.mustUint32(p.keyTaskManualReviewRequired)
}

// AccountRoleGeneral - returns key stored in key/value storage for account role "general"
func (p *KeyValueProvider) AccountRoleGeneral() uint32 {
	return p.mustUint32(p.keyAccountRoleGeneral)
}

// AccountRoleUsVerified - returns key stored in key/value storage for account role "us_verified"
func (p *KeyValueProvider) AccountRoleUsVerified() uint32 {
	return p.mustUint32(p.keyAccountRoleUsVerified)
}

// AccountRoleUsAccredited - returns key stored in key/value storage for account role "us_verified"
func (p *KeyValueProvider) AccountRoleUsAccredited() uint32 {
	return p.mustUint32(p.keyAccountRoleUsAccredited)
}

func (p *KeyValueProvider) mustUint32(key string) uint32 {
	cachedValue, ok := p.entries.Load(key)
	if ok {
		cachedU32Value, ok := cachedValue.(uint32)
		if !ok {
			panic(errors.From(errors.New("failed to convert cached value to uint32"), logan.F{
				"value": cachedU32Value,
			}))
		}
		return cachedU32Value
	}

	response, err := p.keyValueQ.Get(key)
	if err != nil {
		panic(errors.Wrap(err, "failed to get uint32 value", logan.F{
			"key": key,
		}))
	}

	if response.Data.Attributes.Value.Type != xdr.KeyValueEntryTypeUint32 {
		panic(errors.Wrap(err, "value is not uint32 type", logan.F{
			"key":  key,
			"type": response.Data.Attributes.Value.Type,
		}))
	}

	if response.Data.Attributes.Value.U32 == nil {
		panic(errors.Wrap(err, "failed to get uint32 value", logan.F{
			"key":  key,
			"type": response.Data.Attributes.Value.Type,
		}))
	}

	return *response.Data.Attributes.Value.U32
}
