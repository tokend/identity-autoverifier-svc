package client

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
	"time"

	"gitlab.com/distributed_lab/logan/v3/errors"
	legacyKeypair "gitlab.com/tokend/go/keypair"
	"gitlab.com/tokend/go/signcontrol"
	"gitlab.com/tokend/keypair"
)

const jsonAPIContentType = "application/vnd.api+json"

// Client is a wrapper under http.Client that extends it with some signing/throttling/timeouting/predefining headers stuff
type Client struct {
	client  *http.Client
	baseURL *url.URL

	signer  keypair.Full
	headers map[string]string

	throttle chan time.Time
}

// New returns the new instance of Client
func New(baseURL *url.URL, signer keypair.Full) *Client {
	return &Client{
		signer:  signer,
		baseURL: baseURL,
		client: &http.Client{
			Timeout: 2 * time.Minute,
		},
		headers: map[string]string{
			"Content-Type": jsonAPIContentType,
		},
		throttle: throttle(),
	}
}

// WithSigner adds a signer KP to sign every request it performs
func (c Client) WithSigner(signer keypair.Full) Client {
	c.signer = signer
	return c
}

// LegacySigner returns and old-version keypair to be compatible with sigcontrol
func (c Client) LegacySigner() legacyKeypair.KP {
	return legacyKeypair.MustParse(c.signer.Seed())
}

// Post performs the POST http request to the provided endpoint
func (c *Client) Post(endpoint string, body io.Reader) ([]byte, error) {
	u, err := c.resolveURL(endpoint)
	if err != nil {
		return nil, errors.Wrap(err, "failed to resolve url")
	}

	request, err := http.NewRequest(http.MethodPost, u, body)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build POST request")
	}

	return c.do(request)
}

// Get performs the GET http request to the provided endpoint
func (c *Client) Get(endpoint string) ([]byte, error) {
	u, err := c.resolveURL(endpoint)
	if err != nil {
		return nil, errors.Wrap(err, "failed to resolve url")
	}

	request, err := http.NewRequest(http.MethodGet, u, nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build request")
	}

	return c.do(request)
}

func (c *Client) do(request *http.Request) ([]byte, error) {
	<-c.throttle

	for k, v := range c.headers {
		request.Header.Set(k, v)
	}

	if c.signer != nil {
		err := signcontrol.SignRequest(request, c.LegacySigner())
		if err != nil {
			return nil, errors.Wrap(err, "failed to sign request")
		}
	}

	response, err := c.client.Do(request)
	if err != nil {
		return nil, errors.Wrap(err, "failed to perform request")
	}

	defer response.Body.Close()
	bodyBB, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read response body")
	}

	switch {
	case response.StatusCode == http.StatusOK, response.StatusCode == http.StatusCreated:
		return bodyBB, nil
	case response.StatusCode == http.StatusNotFound, response.StatusCode == http.StatusNoContent:
		return nil, newNotFoundError(response, request, bodyBB)
	case response.StatusCode >= http.StatusBadRequest && response.StatusCode < http.StatusInternalServerError:
		return nil, newNetworkError(response, request, bodyBB)
	case response.StatusCode >= http.StatusInternalServerError:
		return nil, newServerError(response, request, bodyBB)
	default:
		return nil, newNetworkError(response, request, bodyBB)
	}
}

func (c *Client) resolveURL(endpoint string) (string, error) {
	u, err := url.Parse(endpoint)
	if err != nil {
		return "", errors.Wrap(err, "failed to parse endpoint")
	}

	// To handle /_/api/ prefixes, because net/url thinks it is a path
	u.Path = path.Join(c.baseURL.Path, u.Path)

	return c.baseURL.ResolveReference(u).String(), nil
}
