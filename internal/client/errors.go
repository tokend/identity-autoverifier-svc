package client

import (
	"fmt"
	"net/http"
)

type NetworkError struct {
	status int
	path   string
	body   string
}

func newNetworkError(response *http.Response, request *http.Request, body []byte) NetworkError {
	return NetworkError{
		status: response.StatusCode,
		path:   request.URL.String(),
		body:   string(body),
	}
}

func (e NetworkError) Error() string {
	return fmt.Sprintf(
		"request failed with status %d, path: %s, body: \n %s \n",
		e.status,
		e.path,
		e.body,
	)
}

type ServerError struct {
	NetworkError
}

func newServerError(response *http.Response, request *http.Request, body []byte) ServerError {
	return ServerError{
		newNetworkError(response, request, body),
	}
}

type NotFoundError struct {
	NetworkError
}

func newNotFoundError(response *http.Response, request *http.Request, body []byte) NotFoundError {
	return NotFoundError{
		newNetworkError(response, request, body),
	}
}
