package config

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

type KeyValue struct {
	AccountRoleGeneral           string `fig:"account_role_general,required"`
	AccountRoleUsVerified        string `fig:"account_role_us_verified"`
	AccountRoleUsAccredited      string `fig:"account_role_us_accredited"`
	TaskSubmitAutoVerification   string `fig:"task_submit_auto_verification,required"`
	TaskCompleteAutoVerification string `fig:"task_complete_auto_verification,required"`
	TaskManualReviewRequired     string `fig:"task_manual_review_required,required"`
}

func NewKeyValue(getter kv.Getter) KeyValue {
	cfg := KeyValue{}

	err := figure.
		Out(&cfg).
		With(figure.BaseHooks).
		From(kv.MustGetStringMap(getter, "key_value")).
		Please()
	if err != nil {
		panic(errors.Wrap(err, "failed to figure out api config"))
	}

	return cfg
}
