package config

import (
	"github.com/spf13/cast"
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"net/url"
	"reflect"
)

func New(getter kv.Getter) Config {
	return Config{
		Api:      NewApi(getter),
		IDMind:   NewIDMind(getter),
		KeyValue: NewKeyValue(getter),
		Runner:   NewRunner(getter),
	}
}

type Config struct {
	Api      Api
	IDMind   IDMind
	KeyValue KeyValue
	Runner   Runner
}

var URLHook = figure.Hooks{
	"*url.URL": func(value interface{}) (reflect.Value, error) {
		str, err := cast.ToStringE(value)
		if err != nil {
			return reflect.Value{}, errors.Wrap(err, "failed to parse string")
		}
		u, err := url.Parse(str)
		if err != nil {
			return reflect.Value{}, errors.Wrap(err, "failed to parse url")
		}
		return reflect.ValueOf(u), nil
	},
}
