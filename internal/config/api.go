package config

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/keypair"
	"gitlab.com/tokend/keypair/figurekeypair"
	"net/url"
)

func NewApi(getter kv.Getter) Api {
	cfg := Api{}

	err := figure.
		Out(&cfg).
		With(figure.BaseHooks, figurekeypair.Hooks, URLHook).
		From(kv.MustGetStringMap(getter, "api")).
		Please()
	if err != nil {
		panic(errors.Wrap(err, "failed to figure out api config"))
	}

	return cfg
}

type Api struct {
	URL    *url.URL     `fig:"url,required"`
	Signer keypair.Full `fig:"signer,required"`
	Cursor string       `fig:"cursor,required"`
}
