package config

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"net/url"
)

func NewIDMind(getter kv.Getter) IDMind {
	cfg := IDMind{}

	err := figure.
		Out(&cfg).
		With(figure.BaseHooks, URLHook).
		From(kv.MustGetStringMap(getter, "idmind")).
		Please()
	if err != nil {
		panic(errors.Wrap(err, "failed to figure out idmind config"))
	}

	return cfg
}

type IDMind struct {
	URL     *url.URL `fig:"url,required"`
	AuthKey string   `fig:"auth_key,required"`
}
