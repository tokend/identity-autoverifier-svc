package config

import (
	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"

	"time"
)

func NewRunner(getter kv.Getter) Runner {
	cfg := Runner{}

	err := figure.
		Out(&cfg).
		With(figure.BaseHooks).
		From(kv.MustGetStringMap(getter, "runner")).
		Please()
	if err != nil {
		panic(errors.Wrap(err, "failed to figure out runner config"))
	}

	return cfg
}

type Runner struct {
	SleepTime time.Duration `fig:"sleep_time"`
}
