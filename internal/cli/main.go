package cli

import (
	"context"
	"github.com/urfave/cli"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/config"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/service"
)

func Run(args []string) bool {
	log := logan.New()

	defer func() {
		if rvr := recover(); rvr != nil {
			log.WithRecover(rvr).Error("app panicked")
		}
	}()

	app := cli.NewApp()

	var cfg config.Config

	app.Commands = cli.Commands{
		{
			Name: "run",
			Before: func(_ *cli.Context) error {
				getter, err := kv.FromEnv()
				if err != nil {
					return errors.Wrap(err, "failed to get config")
				}
				cfg = config.New(getter)
				return nil
			},
			Action: func(_ *cli.Context) {
				service.New(cfg, log).Run(context.Background())
			},
		},
	}

	if err := app.Run(args); err != nil {
		log.WithError(err).Error("app finished")
		return false
	}

	return true
}
