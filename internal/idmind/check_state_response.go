package idmind

func (r CheckStateResponse) RejectReason() (string, map[string]string) {
	if r.KYCState != RejectedKYCState {
		return "", nil
	}

	firedRules := r.EDNAScoreCard.FraudPolicyEvaluationResult.FiredRules

	var details map[string]string
	if len(firedRules) > 0 {
		details = make(map[string]string)
		for _, rule := range firedRules {
			details[rule.Name] = rule.Description
		}
	}

	return reasonRejectedKYCState, details
}

func (r CheckStateResponse) IsManualReview() bool {
	return r.EDNAScoreCard.FraudPolicyEvaluationResult.ReportedRule.ResultCode == ManualReviewFraudResult
}

type CheckStateResponse struct {
	KYCState KYCState `json:"state"` // The current state of the KYC. A - Accepted; R - Under Review; D - Rejected

	MTxID string `json:"mtid"` // The transaction id for this KYC. This id should be provided on subsequent updates to the KYC
	TxID  string `json:"tid"`  // The transaction id of the IDMind transaction. Dunno what's the difference between MTxID and TxID.

	EDNAScoreCard EDNAScoreCard `json:"ednaScoreCard"` // The edna score card for the current transaction
	ResultCodes   string        `json:"rcd"`           // The set of result codes from the evaluation of the current transaction
}

func (r CheckStateResponse) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"m_tx_id":         r.MTxID,
		"tx_id":           r.TxID,
		"kyc_state":       r.KYCState,
		"edna_score_card": r.EDNAScoreCard,
		"result_code":     r.ResultCodes,
	}
}

type FraudResult string

const (
	AcceptFraudResult       = "ACCEPT"
	ManualReviewFraudResult = "MANUAL_REVIEW"
	DenyFraudResult         = "DENY"
)

type KYCState string

const (
	AcceptedKYCState    KYCState = "A"
	UnderReviewKYCState KYCState = "R"
	RejectedKYCState    KYCState = "D"
)

type EDNAScoreCard struct {
	FraudPolicyEvaluationResult ExtEvalResult         `json:"er"`  // The result of the fraud policy evaluation for this transaction
	AutomatedReviewResult       AutomatedReviewResult `json:"ar"`  // The result of the automated review policy for this transaction
	TestResults                 []ConditionResult     `json:"sc"`  // The test results for this transaction
	EvaluatedTestResults        []ConditionResult     `json:"etr"` // The evaluated test results for this transaction
}

func (c EDNAScoreCard) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"test_results(ts)":            c.TestResults,
		"evaluated_test_results(etr)": c.EvaluatedTestResults,
		"fraud_policy_evaluation(er)": c.FraudPolicyEvaluationResult,
		"automated_review(ar)":        c.AutomatedReviewResult,
	}
}

type ConditionResult struct {
	Test           string `json:"test"`           // The id of security test or the key of the transaction data to which the condition applied
	Details        string `json:"details"`        // Textual result of the condition
	Fired          bool   `json:"fired"`          // Whether the condition fired or not
	Timestamp      uint64 `json:"ts"`             // The time in milliseconds UTC at which the result was created. Is only present in the result of Consumer and Merchant KYC
	KYCStage       string `json:"stage"`          // The stage during which this result was created. Is only present in the result of Consumer and Merchant KYC
	WaitingForData bool   `json:"waitingForData"` // Indicates that the result is waiting for an asynchronous response from the customer and/or a third party service
}

func (c ConditionResult) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"test":           c.Test,
		"details":        c.Details,
		"fired":          c.Fired,
		"ts":             c.Timestamp,
		"stage":          c.KYCStage,
		"waitingForData": c.WaitingForData,
	}
}

type ExtEvalResult struct {
	FiredRules   []ExtRule `json:"firedRules"`   // If multiple rules fired during evaluation then this is complete set of rules that fired. Otherwise it is absent
	ReportedRule ExtRule   `json:"reportedRule"` // A rule that fired for the current transaction
	Profile      string    `json:"profile"`      // The name of the profile used for evaluation
}

func (r ExtEvalResult) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"firedRules":   r.FiredRules,
		"reportedRule": r.ReportedRule,
		"profile":      r.Profile,
	}
}

type ExtRule struct {
	Name                 string            `json:"name"`        // The rule name
	Description          string            `json:"description"` // Details of the evaluation of this rule for the current transaction
	Details              string            `json:"details"`     // The rule description
	ResultCode           FraudResult       `json:"resultCode"`  // Result of rule. Possible values are: ACCEPT, MANUAL_REVIEW, DENY
	RuleAssertionResults []ConditionResult `json:"testResults"` // The results of the individual assertions of the rule
	RuleId               int               `json:"ruleId"`      // The unique rule identifier
}

func (r ExtRule) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"name":        r.Name,
		"description": r.Description,
		"details":     r.Details,
		"resultCode":  r.ResultCode,
		"testResults": r.RuleAssertionResults,
		"ruleId":      r.RuleId,
	}
}

type ReviewResult string

const (
	ErrorReviewResult         ReviewResult = "ERROR"
	NoPolicyReviewResult      ReviewResult = "NO_POLICY"
	DisabledReviewResult      ReviewResult = "DISABLED"
	FilteredReviewResult      ReviewResult = "FILTERED"
	PendingReviewResult       ReviewResult = "PENDING"
	FailReviewResult          ReviewResult = "FAIL"
	IndeterminateReviewResult ReviewResult = "INDETERMINATE"
	SuccessReviewResult       ReviewResult = "SUCCESS"
)

type AutomatedReviewResult struct {
	Result          ReviewResult `json:"result"`          // Result of rule
	RuleID          string       `json:"ruleId"`          // The unique rule identifier
	RuleName        string       `json:"ruleName"`        // The rule name
	RuleDescription string       `json:"ruleDescription"` // The rule description
}

func (r AutomatedReviewResult) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"result":          r.Result,
		"ruleId":          r.RuleID,
		"ruleName":        r.RuleName,
		"ruleDescription": r.RuleDescription,
	}
}
