package idmind

import (
	"fmt"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/resources"
	"time"
)

// KYCEvaluationParams - defines set of params to be send to IDMind with create account request
type KYCEvaluationParams struct {
	AccountName   string `json:"man"`           // Account Name for the user
	TxID          string `json:"tid,omitempty"` // Transaction Identifier. If not provided, an id will be allocated internally by IDM.
	Email         string `json:"tea"`           // Email address for the user
	FirstName     string `json:"bfn"`           // Billing First Name
	LastName      string `json:"bln"`           // Billing Last Name
	StreetAddress string `json:"bsn"`           // Billing Street. Include house number, street name and apartment number
	Country       string `json:"bco"`           // Billing Country. ISO 3166 Country code of the Billing Address of the transaction, encoded as a String. Default is â€œUSâ€.
	PostalCode    string `json:"bz"`            // Billing Zip / Postal Code
	City          string `json:"bc"`            // Billing City
	State         string `json:"bs"`            // Billing State
	DateOfBirth   string `json:"dob"`           // Applicant's date of birth encoded as an ISO8601 string

	ScanData          string `json:"scanData"`                    // Required if using Document Verification, the document front side image data, Base64 encoded. If provided this will override the configured â€œJumio client integrationâ€. 5MB maximum size.
	BacksideImageData string `json:"backsideImageData,omitempty"` // If using Document Verification, the document back side image data, Base64 encoded. 5MB maximum size.
	DocCountry        string `json:"docCountry"`                  // Required if using Document Verification, the country in which the document was issued in.
	DocType           string `json:"docType"`                     // Required if using Document Verification, the Type of the Document - Passport (PP) | Driver's Licence (DL) | Government issued Identity Card (ID) |Residence Permit (RP) | Utility Bill (UB)
	Profile           string `json:"profile"`                     // The policy profile to be used to evaluate this transaction. Prior to IDMRisk 1.18 this was encoded in the smna and smid fields
}

// NewKYCEvaluationParams - returns new instance of the KYCEvaluationParams
func NewKYCEvaluationParams(kycData resources.KYCData, idDocument IDDocument, email string) (*KYCEvaluationParams, error) {

	if err := validateKYCData(kycData); err != nil {
		return nil, errors.Wrap(err, "invalid KYC data")
	}

	if err := validateIDDocument(idDocument); err != nil {
		return nil, errors.Wrap(err, "invalid ID document")
	}

	err := validateCountryISOCode(kycData.Address.Country)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("failed to convert Country '%s' to ISO", kycData.Address.Country))
	}

	dateOfBirth, err := time.Parse(time.RFC3339, kycData.DateOfBirth)
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse date of birth")
	}

	return &KYCEvaluationParams{
		AccountName: email,

		Email:         email,
		FirstName:     kycData.FirstName,
		LastName:      kycData.LastName,
		StreetAddress: fmt.Sprintf("%s %s", kycData.Address.Line1, kycData.Address.Line2),
		Country:       kycData.Address.Country,
		PostalCode:    kycData.Address.PostalCode,
		City:          kycData.Address.City,
		State:         kycData.Address.State,
		DateOfBirth:   dateOfBirth.Format("2006-01-02"),

		ScanData:          idDocument.FaceB64(),
		BacksideImageData: idDocument.BackB64(),
		DocType:           string(idDocument.Type),
		Profile:           string(idDocument.Profile),
		DocCountry:        kycData.Address.Country,
	}, nil
}
