package idmind

import (
	"bytes"
	"fmt"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"io"
)

// Uploads attached ID document to IDMind application
func (c *Connector) UploadIDDocument(txID string, document IDDocument) error {
	err := c.uploadFile(txID, document.FaceName, bytes.NewReader(document.FaceFile))
	if err != nil {
		return errors.Wrap(err, "failed to upload face file to IDMind")
	}

	if document.BackFile != nil {
		err := c.uploadFile(txID, document.BackName, bytes.NewReader(document.BackFile))
		if err != nil {
			return errors.Wrap(err, "failed to upload back file to IDMind")
		}
	}

	return nil
}

func (c *Connector) uploadFile(txID, fileName string, fileReader io.Reader) error {
	endpoint := fmt.Sprintf("/account/consumer/%s/files", txID)

	// IDMind returns nothing, so we can ignore the response
	_, err := c.client.PostFile(endpoint, fileName, fileReader)
	if err != nil {
		return errors.Wrap(err, "failed to post file", logan.F{
			"tx_id":    txID,
			"endpoint": endpoint,
		})
	}

	return nil
}
