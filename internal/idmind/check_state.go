package idmind

import (
"encoding/json"
"fmt"
"gitlab.com/distributed_lab/logan/v3"
"gitlab.com/distributed_lab/logan/v3/errors"
)

// CheckState checks the state of the KYC evaluation request with provided txID
func (c *Connector) CheckState(txID string) (*CheckStateResponse, error) {
	fields := logan.F{}

	endpoint := fmt.Sprintf("/account/consumer/%s", txID)
	fields["endpoint"] = endpoint

	responseBB, err := c.client.Get(endpoint)
	if err != nil {
		return nil, errors.Wrap(err, "failed to perform the submit KYC evaluation request", fields)
	}

	response := CheckStateResponse{}
	err = json.Unmarshal(responseBB, &response)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal response bytes into CheckStateResponse structure", fields)
	}

	return &response, nil
}
