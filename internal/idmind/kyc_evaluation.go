package idmind

import (
	"encoding/json"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

// SubmitKYCEvaluation sends a KYC evaluation request to IdentityMind.
func (c *Connector) SubmitKYCEvaluation(params KYCEvaluationParams) (*KYCEvaluationResponse, error) {
	fields := logan.F{}
	fields["params"] = params

	paramsBB, err := json.Marshal(params)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal KYCEvaluationParams", fields)
	}

	responseBB, err := c.client.Post("/account/consumer", paramsBB)
	if err != nil {
		return nil, errors.Wrap(err, "failed to perform the submit KYC evaluation request", fields)
	}

	response := KYCEvaluationResponse{}
	err = json.Unmarshal(responseBB, &response)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal response bytes into KYCEvaluationResponse structure", fields)
	}

	return &response, nil
}
