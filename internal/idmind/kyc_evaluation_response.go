package idmind

const (
	reasonDenyFraudResult  = "Verification service denied fraud policy evaluation denied"
	reasonRejectedKYCState = "Verification service rejected the request"

	problemBadReputation        = "User has a bad reputation"
	problemSuspiciousReputation = "User has a suspicious reputation"
)

// KYCEvaluationResponse represents the response from IDMind KYC evaluation request
type KYCEvaluationResponse struct {
	CheckStateResponse

	PolicyResult FraudResult `json:"res"`  // Result of policy evaluation. Combines the result of fraud and automated review evaluations
	Reputation   Reputation  `json:"user"` // The current reputation of the user involved in this transaction

	FraudResult               FraudResult `json:"frp"` // Result of fraud evaluation
	FiredFraudRule            string      `json:"frn"` // The name of the fraud rule that fired
	FiredFraudRuleDescription string      `json:"frd"` // The description of the fraud rule that fired

	ReputationReason string `json:"erd"` // A description of the reason for the User's reputation
}

func (r KYCEvaluationResponse) GetLoganFields() map[string]interface{} {
	return map[string]interface{}{
		"m_tx_id":               r.MTxID,
		"tx_id":                 r.TxID,
		"kyc_state":             r.KYCState,
		"policy_result":         r.PolicyResult,
		"reputation":            r.Reputation,
		"edna_score_card":       r.EDNAScoreCard,
		"fraud_result":          r.FraudResult,
		"fired_fraud_rule":      r.FiredFraudRule,
		"fired_fraud_rule_desc": r.FiredFraudRuleDescription,
		"reputation_reason":     r.ReputationReason,
		"result_code":           r.ResultCodes,
	}
}

// GetImmediateRejectReason - returns immediate reject reason, if IDMind immediately rejected/denied the evaluation
func (r *KYCEvaluationResponse) GetImmediateRejectReason() (string, map[string]string) {
	if r.FraudResult == DenyFraudResult {
		return reasonDenyFraudResult, map[string]string{
			"fired_fraud_rule":             r.FiredFraudRule,
			"fired_fraud_rule_description": r.FiredFraudRuleDescription,
		}
	}

	return r.CheckStateResponse.RejectReason()
}

// ReputationProblem - returns reason of reputation problem, if it exists
func (r *KYCEvaluationResponse) ReputationProblem() string {
	switch r.Reputation {
	case TrustedReputation, UnknownReputation:
		return ""
	case SuspiciousReputation:
		return problemSuspiciousReputation
	case BadReputation:
		return problemBadReputation
	default:
		return ""
	}
}

type Reputation string

const (
	TrustedReputation    Reputation = "TRUSTED"
	UnknownReputation    Reputation = "UNKNOWN"
	SuspiciousReputation Reputation = "SUSPICIOUS"
	BadReputation        Reputation = "BAD"
)
