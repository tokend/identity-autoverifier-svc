package idmind

import (
	"fmt"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/resources"
)

var validDocTypes = map[DocType]struct{}{
	passportDocType:        {},
	drivingLicenseDocType:  {},
	identityCardDocType:    {},
	residencePermitDocType: {},
}

func validateKYCData(data resources.KYCData) error {
	if len(data.FirstName) > 30 {
		return errors.Errorf("FirstName cannot be larger than 30 letters (%s).", data.FirstName)
	}
	if len(data.LastName) > 50 {
		return errors.Errorf("LastName cannot be larger than 50 letters (%s).", data.LastName)
	}
	streetAddress := fmt.Sprintf("%s %s", data.Address.Line1, data.Address.Line2)
	if len(streetAddress) > 100 {
		return errors.Errorf("StreetAddress cannot be larger than 100 letters (%s).", streetAddress)
	}
	if len(data.Address.PostalCode) > 20 {
		return errors.Errorf("PostalCode cannot be larger than 20 letters (%s).", data.Address.PostalCode)
	}
	if len(data.Address.City) > 30 {
		return errors.Errorf("City cannot be larger than 30 letters (%s).", data.Address.City)
	}
	if len(data.Address.State) > 30 {
		return errors.Errorf("State cannot be larger than 30 letters (%s).", data.Address.State)
	}

	return nil
}

func validateIDDocument(document IDDocument) error {
	_, ok := validDocTypes[document.Type]
	if !ok {
		return errors.Errorf("document type (%s) is invalid.", document.Type)
	}

	if document.FaceMime != "image/png" && document.FaceMime != "image/jpeg" {
		return errors.From(errors.New("mime-Type of face file is neither 'image/png' nor 'image/jpeg'"), logan.F{
			"content_type": document.FaceMime,
		})
	}

	if document.BackFile != nil && document.BackMime != "image/png" && document.BackMime != "image/jpeg" {
		return errors.From(errors.New("mime-Type of back file is neither 'image/png' nor 'image/jpeg'"), logan.F{
			"content_type": document.BackMime,
		})
	}

	return nil
}
