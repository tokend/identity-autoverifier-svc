package idmind

import (
	"encoding/base64"
	"fmt"
	"net/http"

	"gitlab.com/tokend/identity-autoverifier-svc/internal/resources"
)

type DocType string
type Profile string

const (
	passportDocType        DocType = "PP"
	drivingLicenseDocType  DocType = "DL"
	identityCardDocType    DocType = "ID"
	residencePermitDocType DocType = "RP"
)

const (
	noAddressProfile  Profile = "NoAddress"
	hasAddressProfile Profile = "HasAddress"
)

// IDDocument represents the Identity Document that is able to be submitted to IDMind
type IDDocument struct {
	Type    DocType
	Profile Profile

	FaceFile []byte
	BackFile []byte
	FaceName string
	BackName string
	FaceMime string
	BackMime string
}

func NewIDDocument(
	docType resources.DocType,
	faceFile, backFile []byte,
	faceName, backName string,
) IDDocument {
	return IDDocument{
		FaceFile: faceFile,
		BackFile: backFile,
		Type:     getDocType(docType),
		Profile:  getProfile(docType),
		FaceMime: http.DetectContentType(faceFile),
		BackMime: http.DetectContentType(backFile),
		FaceName: faceName,
		BackName: backName,
	}
}

// FaceB64 - returns base64 encoded representation of ID document face file to be sent to IDMind
func (d *IDDocument) FaceB64() string {
	return fmt.Sprintf("%s;base64,%s", d.FaceMime, base64.StdEncoding.EncodeToString(d.FaceFile))
}

// BackB64 - returns base64 encoded representation of ID document back file to be sent to IDMind
func (d *IDDocument) BackB64() string {
	if d.BackFile == nil {
		return ""
	}

	return fmt.Sprintf("%s;base64,%s", d.BackMime, base64.StdEncoding.EncodeToString(d.BackFile))
}

func getDocType(providedType resources.DocType) DocType {
	switch providedType {
	case resources.PassportDocType:
		return passportDocType
	case resources.DrivingLicenseDocType:
		return drivingLicenseDocType
	case resources.IdentityCardDocType:
		return identityCardDocType
	case resources.ResidencePermitDocType:
		return residencePermitDocType
	default:
		return ""
	}
}

func getProfile(providedType resources.DocType) Profile {
	switch providedType {
	case resources.DrivingLicenseDocType,
		resources.ResidencePermitDocType,
		resources.IdentityCardDocType:
		return hasAddressProfile
	default:
		return noAddressProfile
	}
}
