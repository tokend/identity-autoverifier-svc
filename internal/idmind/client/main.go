package client

import (
	"bytes"
	"fmt"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"strings"
	"time"
)

// Client is needed to perform http requests to IDMind API
type Client struct {
	url     string
	client  *http.Client
	headers map[string]string
}

func New(url string, apiKey string) *Client {
	return &Client{
		url: url,
		client: &http.Client{
			Timeout: 2 * time.Minute,
		},
		headers: map[string]string{
			"Authorization": fmt.Sprintf("Basic %s", apiKey),
		},
	}
}

// Get - performs a GET http request to the IDMind API
func (c *Client) Get(endpoint string) ([]byte, error) {
	url := c.url + endpoint

	httpReq, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create HTTP request", logan.F{
			"url": url,
		})
	}

	httpReq.Header.Set("Content-Type", "application/json")
	httpReq.Header.Set("Accept", "application/json")

	return c.do(httpReq)
}

// Post - performs a POST http request to the IDMind API
func (c *Client) Post(endpoint string, paramsBB []byte) ([]byte, error) {
	url := c.url + endpoint

	httpReq, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(paramsBB))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create HTTP request", logan.F{
			"url": url,
		})
	}

	httpReq.Header.Set("Content-Type", "application/json")
	httpReq.Header.Set("Accept", "application/json")

	return c.do(httpReq)
}

func (c *Client) PostFile(endpoint string, fileName string, fileReader io.Reader) ([]byte, error) {
	var buffer bytes.Buffer
	bufferWriter := multipart.NewWriter(&buffer)

	// Write file
	fileWriter, err := bufferWriter.CreateFormFile("file", fileName)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create bufferWriter from file")
	}

	_, err = io.Copy(fileWriter, fileReader)
	if err != nil {
		return nil, errors.Wrap(err, "failed to copy file from Reader to Writer")
	}

	err = bufferWriter.Close()
	if err != nil {
		return nil, errors.Wrap(err, "failed to close request bufferWriter")
	}

	httpReq, err := http.NewRequest(http.MethodPost, c.url+endpoint, &buffer)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create http request")
	}

	httpReq.Header.Set("Content-Type", bufferWriter.FormDataContentType())

	return c.do(httpReq)
}

var ErrNotFound = errors.New("IDMind failed to find application")

func (c *Client) do(httpReq *http.Request) ([]byte, error) {
	fields := logan.F{}

	for k, v := range c.headers {
		httpReq.Header.Set(k, v)
	}

	resp, err := c.client.Do(httpReq)
	if err != nil {
		return nil, errors.Wrap(err, "failed to send POST request to IDMind API", fields)
	}
	fields["status_code"] = resp.StatusCode

	defer func() { _ = resp.Body.Close() }()
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read response body bytes", fields)
	}
	fields["response_body"] = string(respBytes)

	respS := string(respBytes)
	fields["response_body_str"] = respS

	// The way of detecting this particular error from IDMind is quite dirty, but they don't have any error codes to parse their errors more strictly.
	if resp.StatusCode == http.StatusBadRequest && strings.Contains(respS, "Failed to find application") {
		return nil, ErrNotFound
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return nil, errors.From(errors.New("unsuccessful response from IDMind"), fields)
	}

	return respBytes, nil
}
