package idmind

import (
	"gitlab.com/tokend/identity-autoverifier-svc/internal/idmind/client"
	"io"
)

type Client interface {
	Get(endpoint string) ([]byte, error)
	Post(endpoint string, paramsBB []byte) ([]byte, error)
	PostFile(endpoint string, fileName string, fileReader io.Reader) ([]byte, error)
}

// Connector is needed to communicate with IDMind API
type Connector struct {
	client Client
}

// New - returns the new instance of Connector
func New(url, authKey string) *Connector {
	return &Connector{
		client: client.New(url, authKey),
	}
}
