package api

import (
	"fmt"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/regources/generated"
)

// ChangeRoleRequestsQ is needed to perform requests to /change_role_requests horizon endpoints
type ChangeRoleRequestsQ struct {
	Base
}

// NewChangeRoleRequestsQ - returns new instance of ChangeRoleRequestsQ
func NewChangeRoleRequestsQ(client Client) ChangeRoleRequestsQ {
	return ChangeRoleRequestsQ{
		Base: base{client},
	}
}

// GetList - perform the GET request to load change role requests list
func (q ChangeRoleRequestsQ) GetList(params QueryParams) (*regources.ReviewableRequestsResponse, error) {
	response := regources.ReviewableRequestsResponse{}

	if err := q.get("v3/change_role_requests", params, &response); err != nil {
		return nil, errors.Wrap(err, "failed to get v3/change_role_requests response")
	}

	return &response, nil
}

// Get - perform the Get request to change role request by ID
func (q ChangeRoleRequestsQ) Get(id uint64) (*regources.ReviewableRequestResponse, error) {
	response := regources.ReviewableRequestResponse{}
	endpoint := fmt.Sprintf("v3/change_role_requests/%d", id)

	if err := q.get(endpoint, nil, &response); err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("failed to get %s response", endpoint))
	}

	return &response, nil
}
