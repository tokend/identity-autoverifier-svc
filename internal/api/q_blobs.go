package api

import (
	"fmt"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/resources"
)

// BlobsQ is needed to perform requests to /blobs identity endpoints
type BlobsQ struct {
	Base
}

// NewBlobsQ - returns new instance of BlobsQ
func NewBlobsQ(client Client) BlobsQ {
	return BlobsQ{
		Base: base{client: client},
	}
}

// Get - performs the GET request to get the blob by ID
func (q BlobsQ) Get(blobId string) (*resources.BlobResponse, error) {
	response := resources.BlobResponse{}
	endpoint := fmt.Sprintf("blobs/%s", blobId)

	if err := q.get(endpoint, nil, &response); err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("failed to get %s response", endpoint))
	}

	return &response, nil
}
