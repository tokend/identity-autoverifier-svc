package api

import (
	"gitlab.com/tokend/identity-autoverifier-svc/internal/resources"
	"net/url"

	"gitlab.com/distributed_lab/logan/v3/errors"
)

// IdentitiesQ is needed to perform requests to /identities identity API endpoint
type IdentitiesQ struct {
	Base
}

// NewIdentitiesQ - returns new instance of IdentitiesQ
func NewIdentitiesQ(client Client) IdentitiesQ {
	return IdentitiesQ{
		Base: base{client},
	}
}

// GetList - perform the GET request to load identities list
func (q IdentitiesQ) GetList(params QueryParams) (*resources.IdentitiesResponse, error) {
	response := resources.IdentitiesResponse{}

	if err := q.get("identities", params, &response); err != nil {
		return nil, errors.Wrap(err, "failed to get identities")
	}

	return &response, nil
}

// GetByEmail - returns identity by it's address
func (q IdentitiesQ) GetByAddress(address string) (*resources.Identity, error) {
	params := url.Values{}
	params.Set("filter[address]", address)

	response, err := q.GetList(params)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get identity list")
	}

	if len(response.Data) == 0 {
		return nil, nil
	}

	return &response.Data[0], nil
}
