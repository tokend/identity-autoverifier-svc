package api

import (
	"bytes"
	"encoding/json"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/resources"
	"gitlab.com/tokend/keypair"
)

// TxSubmitter is used to submit transactions
type TxSubmitter struct {
	client  Client
	signer  keypair.Full
	source  keypair.Address
	builder *xdrbuild.Builder
	log     *logan.Entry
}

// NewTxSubmitter - returns new instance of TxSubmitter
func NewTxSubmitter(
	client Client,
	signer keypair.Full,
	source keypair.Address,
	builder *xdrbuild.Builder,
	log *logan.Entry,
) *TxSubmitter {

	return &TxSubmitter{
		client:  client,
		signer:  signer,
		source:  source,
		builder: builder,
		log:     log,
	}
}

func (s *TxSubmitter) Submit(operations ...xdrbuild.Operation) error {
	tx := s.builder.Transaction(s.source)

	for _, operation := range operations {
		tx = tx.Op(operation)
	}

	envelope, err := tx.Sign(s.signer).Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to marshal tx")
	}

	var buf bytes.Buffer
	err = json.NewEncoder(&buf).Encode(&resources.TxData{
		Tx: envelope,
	})
	if err != nil {
		return errors.Wrap(err, "failed to marshal request")
	}

	response, err := s.client.Post("transactions", &buf)

	if err != nil {
		return err
	}

	var success resources.TxSuccessResponse

	if err := json.Unmarshal(response, &success); err != nil {
		// oops, tx was successful but we failed to unmarshal response.
		// let's ignore error and hope nothing will break
		s.log.
			WithField("response", string(response)).
			Warn("transaction submitted successfully, but failed to unmarshal response")
	}

	return nil
}
