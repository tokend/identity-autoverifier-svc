package api

import (
	"fmt"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/regources/generated"
)

// KeyValueQ is needed to perform requests to /v3/key_values horizon endpoints
type KeyValueQ struct {
	Base
}

// NewKeyValueQ - returns new instance of KeyValueQ
func NewKeyValueQ(client Client) KeyValueQ {
	return KeyValueQ{
		Base: base{client: client},
	}
}

// Get performs the get request to get k/v entry by it's key
func (q KeyValueQ) Get(key string) (*regources.KeyValueEntryResponse, error) {
	response := regources.KeyValueEntryResponse{}
	endpoint := fmt.Sprintf("/v3/key_values/%s", key)

	if err := q.get(endpoint, nil, &response); err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("failed to get %s response", endpoint))
	}

	return &response, nil
}
