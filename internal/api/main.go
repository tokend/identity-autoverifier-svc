package api

import "io"

type Client interface {
	Get(endpoint string) ([]byte, error)
	Post(endpoint string, body io.Reader) ([]byte, error)
}

type Base interface {
	get(endpoint string, params QueryParams, dest interface{}) error
}
