package api

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"io"
	"testing"
)

type mockedClient struct {
	mock.Mock
}

func (c *mockedClient) Get(endpoint string) ([]byte, error) {
	args := c.Called(endpoint)

	if args.Get(0) != nil {
		return args.Get(0).([]byte), args.Error(1)
	}

	return nil, args.Error(1)
}

func (c *mockedClient) Post(endpoint string, body io.Reader) ([]byte, error) {
	args := c.Called(endpoint, body)

	if args.Get(0) != nil {
		return args.Get(0).([]byte), args.Error(1)
	}

	return nil, args.Error(1)
}

func TestBase_get(t *testing.T) {
	t.Run("should call client.Get with proper params", func(t *testing.T) {
		client := new(mockedClient)
		client.
			On("Get", "some-endpoint?foo=bar&fizz=buzz").
			Return([]byte(`{ "first_name": "John", "last_name": "Doe" }`), nil)

		b := base{client}

		err := b.get("some-endpoint", testQueryParams{}, &testDest{})

		client.AssertCalled(t, "Get", "some-endpoint?foo=bar&fizz=buzz")
		assert.NoError(t, err)
	})

	t.Run("should return error if client returned nil", func(t *testing.T) {
		client := new(mockedClient)
		client.
			On("Get", "some-endpoint?foo=bar&fizz=buzz").
			Return(nil, nil)

		b := base{client}

		err := b.get("some-endpoint", testQueryParams{}, &testDest{})

		assert.Error(t, err)
	})
}

type testDest struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type testQueryParams struct{}

func (p testQueryParams) Encode() string {
	return "foo=bar&fizz=buzz"
}
