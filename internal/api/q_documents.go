package api

import (
	"fmt"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/identity-autoverifier-svc/internal/resources"
)

// DocumentsQ is needed to perform requests to /documents identity endpoints
type DocumentsQ struct {
	Base
}

// NewDocumentsQ - returns new instance of DocumentsQ
func NewDocumentsQ(client Client) DocumentsQ {
	return DocumentsQ{
		Base: base{client: client},
	}
}

// Get performs the Get request to get document URL by document ID
func (q DocumentsQ) GetUrl(documentId string) (*resources.DocumentURLResponse, error) {
	response := resources.DocumentURLResponse{}

	endpoint := fmt.Sprintf("documents/%s", documentId)

	if err := q.get(endpoint, nil, &response); err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("failed to get %s response", endpoint))
	}

	return &response, nil
}
