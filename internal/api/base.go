package api

import (
	"encoding/json"
	"fmt"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

type base struct {
	client Client
}

type QueryParams interface {
	Encode() string
}

func (b base) get(endpoint string, params QueryParams, dest interface{}) error {
	p := ""
	if params != nil {
		p = params.Encode()
	}
	u := fmt.Sprintf("%s?%s", endpoint, p)

	rawResponse, err := b.client.Get(u)
	if err != nil {
		return errors.Wrap(err, "failed to perform get request")
	}

	err = json.Unmarshal(rawResponse, dest)
	if err != nil {
		return errors.Wrap(err, "failed to unmarshal change role requests response")
	}

	return nil
}
