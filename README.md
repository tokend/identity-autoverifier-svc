# identity-autoverifier-svc

## Requirements:

* Golang 1.10.8

## Run

To run a service, please set idmind and api(horizon) configs in config.yaml file.
If you want to run it with TokenD [developer edition](https://github.com/tokend/developer-edition), you may leave api configs without any changes, everything is already set up to work with it.

After configuration step, please execute:

```bash

# Show the service where to take config:
export KV_VIPER_FILE=/path/to/your/config.yaml

# Build the service:
go build -o ./identity-auto-veririer

# Run the service
./identity-auto-verifier run

```

## Prerequisites

Such key/value entries should exist

* `account_role:general`
* `account_role:us_verified`
* `account_role:us_accredited`
* `change_role_task:submit_auto_verification`
* `change_role_task:complete_auto_verification`
* `change_role_task:manual_review_required`

For service to start processing the request it should have `change_role_task:submit_auto_verification` in it's pending 
tasks. This can be achieved by configuring the key/value entry `change_role_tasks:<role_from>:<role_to>` to set default 
tasks automatically or manually set it via review request operation.

